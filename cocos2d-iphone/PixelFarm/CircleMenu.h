//
//  CircleMenu.h
//  PixelFarm
//
// Copyright (c) 2012 Thomas Bruno
// Copyright (c) 2012 NaveOSS.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "cocos2d.h"
#import "Constants.h"

@class CircleMenu;

@protocol CircleMenuProtocol <NSObject>


-(void)circleMenuToolWasSelected:(ToolTypes)tool sender:(CircleMenu*)circleMenu;
-(void)circleMenuWasOpened;
-(void)circleMenuWasClosed;
-(void)circleMenuShouldCloseWithSender:(CircleMenu*)circleMenu;

@end

@interface CircleMenu : CCLayer {
	CGFloat menuRadius;
	BOOL open;

}

@property (weak,nonatomic) id <CircleMenuProtocol> delegate;

-(void)hideMenuWithAnimation:(BOOL)animated duration:(ccTime)time;
-(void)showMenuWithAnimation:(BOOL)animated duration:(ccTime)time;
-(BOOL)isOpen;

@end
