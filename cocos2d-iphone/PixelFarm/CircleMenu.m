//
//  CircleMenu.m
//  PixelFarm
//
// Copyright (c) 2012 Thomas Bruno
// Copyright (c) 2012 NaveOSS.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "CircleMenu.h"
#import "GameUtil.h"

#define SquareUsable 1000
#define SquareUnUsable 1001
#define SeedButtonCorn 1002
#define SeedButtonCarrot 1003
#define SeedButtonCucumber 1004
#define SeedButtonBroccoli 1005
#define SeedButtonPotato 1006
#define SeedButtonRedPepper 1007
#define SeedButtonTomato 1008
#define SquareUsableIcon 1009
#define ToggleButtonIcon 1010
#define ToolButtonHoe 1011

@implementation CircleMenu


- (id)init
{
    self = [super init];
    if (self) {
		menuRadius = 192.0f;
		open = false;
		[self setupButtons];
		[self scheduleUpdate];
		[self setVisible:false];
    }
    return self;
}


-(void)openMenuWithDuration:(ccTime)time {
	int count = [[self children] count];
	CCLOG(@"Opening Menu with Duration: %f:",time);
//	CCLOG(@"Angle was: %f",angle);

	for(int i = 0; i < count; i++) {
		CGFloat degree = (360/count) * i + 32;
		CCLOG(@"Angle Was: %f",degree);

		CGFloat angleRads = [GameUtil DegToRad:degree];
		float x = 0 + 92 * cos(angleRads);
		float y = 0 + 92 * sin(angleRads);

		CCSprite *sprite = [self.children objectAtIndex:(i)];
		CGPoint pos = ccp(x,y);

		[sprite runAction:[CCMoveTo actionWithDuration:time position:pos]];
	}
	
	self.isTouchEnabled = YES;
	open = true;
	[self openMenu];
}

-(void)closeMenuWithDuration:(ccTime)time {
	for(int i = 0; i < [[self children] count]; i++) {
		CCSprite *sprite = [self.children objectAtIndex:i];
		CGPoint pos = ccp(0,0);
		[sprite runAction:[CCSequence actions:[CCMoveTo actionWithDuration:time position:pos],[CCCallFunc actionWithTarget:self selector:@selector(closeMenu)],nil]];
	}
	self.isTouchEnabled = NO;
	open = false;
}


-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	CCLOG(@"Got Touch For CircleMenu");
	bool touchOnSelf = false;
	for(UITouch *touch in touches) {
		CGPoint loc = [touch locationInView:[touch view]];
		loc = [[CCDirector sharedDirector] convertToGL:loc];
		CGPoint localLoc = [self convertToNodeSpace:loc];
		for (CCSprite *sprite in [self children]) {
			if(CGRectContainsPoint([sprite boundingBox], localLoc) && touchOnSelf != true) {
				CCLOG(@"Touched a Tool!");
				[self toolSelected:[sprite tag]];
				touchOnSelf = true;
			}
		}
	}


	if(touchOnSelf != true) {
		[self.delegate circleMenuShouldCloseWithSender:self];
	}
}

-(BOOL)isOpen {
	return open;
}


-(void)update:(ccTime)delta {
	
}


-(void)toolSelected:(int)tag {
	CCLOG(@"Running Tool Selected. Sending message to delegate");
	switch(tag) {
		case SeedButtonBroccoli:
			[self.delegate circleMenuToolWasSelected:kToolPlantBroccoli sender:self];
			break;
		case SeedButtonCarrot:
			[self.delegate circleMenuToolWasSelected:kToolPlantCarrot sender:self];
			break;
		case SeedButtonCorn:
			[self.delegate circleMenuToolWasSelected:kToolPlantCorn sender:self];
			break;
		case SeedButtonCucumber:
			[self.delegate circleMenuToolWasSelected:kToolPlantCucumber sender:self];
			break;
		case SeedButtonPotato:
			[self.delegate circleMenuToolWasSelected:kToolPlantPotato sender:self];
			break;
		case SeedButtonRedPepper:
			[self.delegate circleMenuToolWasSelected:kToolPlantRedPepper sender:self];
			break;
		case SeedButtonTomato:
			[self.delegate circleMenuToolWasSelected:kToolPlantTomato sender:self];
			break;
		case ToolButtonHoe:
			[self.delegate circleMenuToolWasSelected:kToolHoe sender:self];
			break;
		default:
			CCLOG(@"Tool that was selected was not found! %d",tag);
	}
}


-(void)hideMenuWithAnimation:(BOOL)animated duration:(ccTime)time {

	if(animated ) {
		[self closeMenuWithDuration:time];
	} else {
		[self closeMenuWithDuration:0.0f];
	}
}

-(void)openMenu {
	[self.delegate circleMenuWasOpened];
	[self setVisible:true];
}

-(void)closeMenu {
	[self.delegate circleMenuWasClosed];
	[self setVisible:false];
}

-(void)showMenuWithAnimation:(BOOL)animated duration:(ccTime)time {
	if(animated) {
		[self openMenuWithDuration:time];
	} else {
		[self openMenuWithDuration:time];
	}
}


-(void)setupButtons {
	
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"CarrotSeedButtonUp.png"] z:0 tag:SeedButtonCarrot];
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"CornSeedButtonUp.png"] z:0 tag:SeedButtonCorn];
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"BroccoliSeedButtonUp.png"] z:0 tag:SeedButtonBroccoli];
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"PotatoSeedButtonUp.png"] z:0 tag:SeedButtonPotato];
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"RedPepperSeedButtonUp.png"] z:0 tag:SeedButtonRedPepper];
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"CucumberSeedButtonUp.png"] z:0 tag:SeedButtonCucumber];
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"TomatoSeedButtonUp.png"] z:0 tag:SeedButtonTomato];
	[self addChild:[CCSprite spriteWithSpriteFrameName:@"PlowButtonUp.png"] z:0 tag:ToolButtonHoe];


	[[self getChildByTag:SeedButtonCarrot] setPosition:ccp(0,0)];
	[[self getChildByTag:SeedButtonCorn] setPosition:ccp(0,0)];
	[[self getChildByTag:SeedButtonBroccoli] setPosition:ccp(0,0)];
	[[self getChildByTag:SeedButtonPotato] setPosition:ccp(0,0)];
	[[self getChildByTag:SeedButtonRedPepper] setPosition:ccp(0,0)];
	[[self getChildByTag:SeedButtonCucumber] setPosition:ccp(0,0)];
	[[self getChildByTag:SeedButtonTomato] setPosition:ccp(0,0)];
	[[self getChildByTag:ToolButtonHoe] setPosition:ccp(0,0)];

	[[(CCSprite*)[self getChildByTag:SeedButtonCarrot] texture] setAliasTexParameters];
	[[(CCSprite*)[self getChildByTag:SeedButtonCorn] texture] setAliasTexParameters];
	[[(CCSprite*)[self getChildByTag:SeedButtonBroccoli] texture] setAliasTexParameters];
	[[(CCSprite*)[self getChildByTag:SeedButtonPotato] texture] setAliasTexParameters];
	[[(CCSprite*)[self getChildByTag:SeedButtonRedPepper] texture] setAliasTexParameters];
	[[(CCSprite*)[self getChildByTag:SeedButtonCucumber] texture] setAliasTexParameters];
	[[(CCSprite*)[self getChildByTag:SeedButtonTomato] texture] setAliasTexParameters];
	[[(CCSprite*)[self getChildByTag:ToolButtonHoe] texture] setAliasTexParameters];
}

@end
