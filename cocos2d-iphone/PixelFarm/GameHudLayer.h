//
//  GameHudLayer.h
//  PixelFarm
//
//  Created by Thomas Bruno on 8/8/12.
//  Copyright (c) 2012 NaveOSS. All rights reserved.
//
#import "cocos2d.h"
#import "Constants.h"
#import "CCLayer.h"
#import "ToggleButton.h"
#include "CircleMenu.h"

@class GameLayer;
@class ToolCursor;

@interface GameHudLayer : CCLayer <CircleMenuProtocol> {
	__weak GameLayer *gameLayer;
	ToolCursor *cursorTool;

	CGPoint selectedPos;

	ToolTypes currentTool;
	PlantTypes currentSeed;
	
	NSMutableArray *seedArray;
	
	BOOL gridActive;
	BOOL closeNextTouch;
	
}

@property (weak,nonatomic) GameLayer *gameLayer;

-(id)initGameHudWithGameLayer:(GameLayer*)gl;


@end
