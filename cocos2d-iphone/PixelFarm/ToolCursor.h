//
//  ToolCursor.h
//  PixelFarm
//
//  Created by Thomas Bruno on 8/10/12.
//  Copyright (c) 2012 NaveOSS. All rights reserved.
//
#import "cocos2d.h"

#import "CCSprite.h"
#import "Constants.h"

@interface ToolCursor : CCSprite {
	CCSprite *toolIcon;
	ToolTypes currentTool;

	bool isUsable;
}

@property (readonly) bool isUsable;

-(void)setCursorIndicator:(bool)usable;

-(void)setCurrentTool:(ToolTypes)currentTool;
-(ToolTypes)currentTool;



@end
