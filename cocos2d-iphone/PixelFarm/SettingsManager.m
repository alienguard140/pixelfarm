//
//  SettingsManager.m
//
//  http://getsetgames.com/2009/10/07/saving-and-loading-user-data-and-preferences/
//

#import "SettingsManager.h"

@implementation SettingsManager

-(NSString *)getString:(NSString*)value
{	
	return [settings objectForKey:value];
}

-(NSString *)getString:(NSString*)value defaultString:(NSString*)defaultValue
{      
    NSString *result = [settings objectForKey:value];
    if(result == nil)
        result = defaultValue;
    
    return result;
    
}

-(int)getInt:(NSString*)value {
	return [[settings objectForKey:value] intValue];
}

-(int)getInt:(NSString*)value defaultInt:(int)defaultValue {
       int result = -1;
       
       if([settings objectForKey:value] != nil)
               result = [[settings objectForKey:value] intValue];
       else 
               result = defaultValue;

       return result;
}

-(void)setValue:(NSString*)value newString:(NSString *)aValue {	
	[settings setObject:aValue forKey:value];
}

-(void)setValue:(NSString*)value newInt:(int)aValue {
	[settings setObject:[NSString stringWithFormat:@"%i",aValue] forKey:value];
}

-(void)save
{
	// NOTE: You should be replace "MyAppName" with your own custom application string.
	//
	[[NSUserDefaults standardUserDefaults] setObject:settings forKey:@"PixelFarm"];
	[[NSUserDefaults standardUserDefaults] synchronize];	
}

-(void)load
{
	// NOTE: You should be replace "MyAppName" with your own custom application string.
	//
	[settings addEntriesFromDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"PixelFarm"]];
}

// Logging function great for checkin out what keys/values you have at any given time
//
-(void)logSettings
{
	for(NSString* item in [settings allKeys])
	{
		NSLog(@"[SettingsManager KEY:%@ - VALUE:%@]", item, [settings valueForKey:item]);
	}
}

+(id)sharedSettingsManager {
	static dispatch_once_t pred;
	static SettingsManager *sharedmanager = nil;
	
	dispatch_once(&pred, ^{ sharedmanager = [[self alloc] init]; });
	return sharedmanager;
}

-(id)init {	
	settings = [[NSMutableDictionary alloc] initWithCapacity:5];	
	return [super init];
}

@end