//
// Plant.m
// PixelFarm
//
// Created by Thomas Bruno on 7/31/12.
// Copyright (c) 2012 Thomas Bruno
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "cocos2d.h"
#import "Plant.h"
#import "GameObject.h"

@interface Plant()
	
-(void)loadPlantFrames:(PlantTypes)ptype;

@end


@implementation Plant


-(id)initWithPlantType:(PlantTypes)ptype {
	self = [super init];
	if(self != nil) {
		_type = ptype;
		spriteFrameArray = [NSMutableArray array];
		[self loadPlantFrames:ptype];
		currentStage = 0;
		growRate = 5;
		growTimer = growRate;
		
		[self setAnchorPoint:ccp(0.5f,0.0f)];
		[self setDisplayFrame:[spriteFrameArray objectAtIndex:0]];
		
		[self scheduleUpdate];
	}
	
	return self;
}

-(void)update:(ccTime)delta {
	if(growTimer < 0.0f) {
		growTimer = growRate;
		[self grow];
	} else {
		growTimer -= delta;
	}
}


-(int)stages {
	return [spriteFrameArray count];
}

-(void)grow {
	if(currentStage < [spriteFrameArray count] - 2) {
		currentStage++;
		[self setDisplayFrame:[spriteFrameArray objectAtIndex:currentStage]];
	}
}


-(void)loadPlantFrames:(PlantTypes)ptype {
	CCSpriteFrameCache *cache = [CCSpriteFrameCache sharedSpriteFrameCache];
	
	switch (ptype){
		case kPlantCorn:
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-corn01.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-corn02.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-corn03.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-corn04.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-corn05.png"]];
			self.offset = ccp(1,-5);
			break;
		case kPlantBroccoli:
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-broccoli01.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-broccoli02.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-broccoli03.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-broccoli04.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-broccoli05.png"]];
			self.offset = ccp(2,-5);
			break;
		case kPlantTomato:
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-tomato01.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-tomato02.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-tomato03.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-tomato04.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-tomato05.png"]];
			self.offset = ccp(2,-5);
			break;
		case kPlantCarrot:
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-carrot01.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-carrot02.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-carrot03.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-carrot04.png"]];
//			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-groundharvested.png"]];
			self.offset = ccp(2,-5);
			break;
		case kPlantRedPepper:
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-redpepper01.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-redpepper02.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-redpepper03.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-redpepper04.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-redpepper05.png"]];
			self.offset = ccp(2,-3);
			break;
		case kPlantPotato:
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-potato01.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-potato02.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-potato03.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-potato04.png"]];
//			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-groundharvested.png"]];
			self.offset = ccp(1,-3);
			break;
		case kPlantCucumber:
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-cucumber01.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-cucumber02.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-cucumber03.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-cucumber04.png"]];
			[spriteFrameArray addObject:[cache spriteFrameByName:@"growth-cucumber05.png"]];
			self.offset = ccp(2,-5);
			break;
	}
}

+(Plant*)plantWithPlantType:(PlantTypes)ptype at:(CGPoint)position withParent:(CCNode*)parent {
	CGSize winSize = [[CCDirector sharedDirector] winSize];
	
	Plant *plant = [[Plant alloc] initWithPlantType:ptype];
	if(plant != nil) {
		[plant setPosition:ccpAdd(position,plant.offset)];
		[parent addChild:plant z:(winSize.height - position.y)];
	}
	return plant;
}

@end