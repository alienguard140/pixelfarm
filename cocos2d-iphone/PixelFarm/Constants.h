//
// Constants.h
// PixelFarm
//
// Created by Thomas Bruno on 7/28/12.
//
// Copyright (c) 2012 Thomas Bruno
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#ifndef PixelFarm_Constants_h
#define PixelFarm_Constants_h

typedef enum { kSceneUninitialized, kSceneMainMenu, kSceneTutorial, kSceneFarm, kSceneGameOver, kSceneCredits } GameScenes;
typedef enum { kGameTrack1 } MusicTracks;
typedef enum { kSound1 } SoundEffects;

typedef enum { kPlantNone, kPlantCucumber, kPlantCorn, kPlantRedPepper, kPlantPotato, kPlantTomato, kPlantBroccoli, kPlantCarrot } PlantTypes;
typedef enum { kPlantStagePlanted, kPlantStageGrowing, kPlantStageHarvested } PlantStages;
typedef enum { kToolNone, kToolBuy, kToolSell, kToolHoe, kToolHarvest, kToolPlantCucumber, kToolPlantCorn, kToolPlantRedPepper, kToolPlantPotato, kToolPlantTomato, kToolPlantBroccoli, kToolPlantCarrot } ToolTypes;

typedef enum { kDirectionNorthWest, kDirectionNorth, kDirectionNorthEast, kDirectionWest, kDirectionNone, kDirectionEast, kDirectionSouthWest,kDirectionSouth,kDirectionSouthEast } Direction;
typedef enum { kTileNone,

	kTileEdgeN = 2,
	kTileEdgeE = 18,
	kTileEdgeW = 16,
	kTIleEdgeS = 32,

	kTileEdgeWE = 19,
	kTileEdgeNS = 6,

	kTileCornerNW = 1,
	kTileCornerNE = 3,
	kTileCornerSW = 31,
	kTileCornerSE = 33,

	kTileCornerNWNE = 4,
	kTileCornerNWSW = 5,
	kTileCornerNESE = 7,
	kTileCornerNWSE = 20,
	kTileCornerNESW = 21,
	kTileCornerSWSE = 34,

	kTileCornerNESWSE = 46,
	kTileCornerNWNESE = 47,
	kTileCornerNWNESW = 48,
	kTileCornerNWSWSE = 61,

	kTileCornerNWNESWSE = 49,

	kTileCornerSE_EdgeN = 8,
	kTileCornerSWSE_EdgeN = 9,
	kTileCornerSW_EdgeN = 10,

	kTileCornerNE_EdgeS = 23,
	kTileCornerNWNE_EdgeS = 24,
	kTileCornerNW_EdgeS = 25,

	kTileCornerNW_EdgeE =35,
	kTileCornerNWSW_EdgeE = 36,
	kTileCornerSW_EdgeE = 37,

	kTileCornerSE_EdgeW = 50,
	kTileCornerNESE_EdgeW = 51,
	kTileCornerNE_EdgeW = 52,

	kTileHole = 11,
	kTileRock = 12,
	kTileCenter = 17,
	kTileGrid = 22,

	kTileGrass1 = 28,
	kTileGrass2 = 13,
	kTileGrass3 = 26,
	kTileGrass4 = 27
} GameTiles;


#endif
